from flask import request
from flask import Blueprint
from flask_restful import Resource
import numpy as np
from PIL import Image
import requests
import json
import logging
from config import read_config

Sort = Blueprint('sort', __name__)
config = read_config()
@Sort.route('')
class Sort(Resource):    
    def post(self): #post 요청시 json 형태로 반환
        
        # key=files values stream으로 처리
        # RGB, 사이즈 전처리
        img = np.array(Image.open(request.files['file'].stream).convert('RGB').resize((256, 256))).tolist() 
        
        #응답값 파일명 처리를 위한 선언
        img_name =request.files['file'].filename
        
        try:
            # tf모델 호출 준비
            data = {"signature_name": "serving_default", "instances": [img]}
            headers = {"content-type": "application/json"}
            response = requests.post(config['modelUri'], json=data)    
            # 모델 추론
            predict =  json.loads(response.text)['predictions']
            
            # 결과 처리
            scores = np.array(predict)
            typeCode = str(scores.argmax())
            typeName= config['documentation_index'].get(typeCode)
            typeRate = str(round(scores.max()*100,2))
            print(typeName)
            print('typeCode',typeCode)
            print('score',scores)

            # 응답값 반환
            return {
                            'fileName':img_name,
                            'typeName': typeName,
                            'typeRate':typeRate,
                            'typeCode':typeCode
                        }

        except Exception as e:
            logging.error(f"{e} : Return result to manually classification as raising error")
            return {
                            'fileName':img_name,
                            'typeName':'No match',
                            'typeRate':0,
                            'typeCode':-1
                        }
        
        
        