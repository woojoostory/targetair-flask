from flask import Flask
from flask_restful import Api
from flask_cors import CORS

from sort import Sort
from ocr_bf import Ocr_BF
from ocr_ktbiz import Ocr_KtBiz


# Flask 객체 선언
app = Flask(__name__)
# Flask 객체 API 객체 등록
api = Api(app)
CORS(app)

#URI 처리
api.add_resource(Sort, '/sort')

## 사업자 등록증 OCR URL 추가
api.add_resource(Ocr_BF, '/ocr_bf')

## KT Biz 신청서 OCR URL 추가
api.add_resource(Ocr_KtBiz, '/ocr_ktbiz')

if __name__ =='__main__':
    app.run(host='0.0.0.0',port='5000',debug=True)
    
