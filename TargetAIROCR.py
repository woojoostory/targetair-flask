import cv2 as cv
import numpy as np
from pytesseract import image_to_string

import os
import time
import math
from operator import itemgetter
import logging


def getText(img, lang_script = 'script/Hangul', psm = '6', oem = '1', xywh = ''):

    rst_txt = ""
    print("rst_txt0", rst_txt)
    try:

        if xywh != '':

            x = int(xywh.split(':')[0])
            y = int(xywh.split(':')[1])
            w = int(xywh.split(':')[2])
            h = int(xywh.split(':')[3])
            
            img = img[y : y + h, x : x + w]

        rst_txt = image_to_string(img, lang = lang_script, config = '--psm '+ psm +' --oem '+ oem)
        

    except Exception as ex:

            logging.error(f"{ex} : err")
            return "ERR"
        
    return rst_txt


def removeWaterMark(img):

    rst_img = []

    try:

        #alpha = 2.0
        alpha = 2.0
        
        #beta = -160
        beta = -160

        rst_img = alpha * img + beta
        
        rst_img = np.clip(rst_img, 0, 255).astype(np.uint8)

    except Exception as ex:

        rst_img = img

        print("TargetAIROCR.removeWaterMark", ex)
        
    return rst_img


def getDetectTextLocation(img, kernel_size = (1, 35), w_limit = (0, 30), h_limit = (0, 35), thr = 250):

    rst_lst = []

    try:

        #img = removeWaterMark(img)
        
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

        blur = cv.GaussianBlur(gray, (5, 5), 0)
        #blur = gray
        
        _, thr_img = cv.threshold(blur, thr, 255, cv.THRESH_BINARY)

        kernel = np.ones(kernel_size, np.uint8)
        mask = cv.morphologyEx(thr_img, cv.MORPH_OPEN, kernel)
        mask = cv.bitwise_not(mask)
        
        contours, hierarchy = cv.findContours(mask, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE)

        for contour in contours:
            
            peri = cv.arcLength(contour, True)
            x, y, w, h = cv.boundingRect(contour)

            if (h > h_limit[0] and h <= h_limit[1]) and (w > w_limit[0] and w <= w_limit[1]):
                
                rst_lst.append([x, y, w, h])

    except Exception as ex:

        print("TargetAIROCR.getDetectTextLocation", ex)
        
    return rst_lst, mask


def detectText(img, kernel_size = (1, 35), w_limit = (0, 30), h_limit = (0, 35), thr = 250):

    rst_img = []

    try:

        txt_ares, _ = getDetectTextLocation(img, kernel_size, w_limit, h_limit, thr)

        txt_ares = sorted(txt_ares, key = itemgetter(1, 0))

        tmp_img = np.ones((img.shape[0], img.shape[1], img.shape[2]), np.uint8) * 255
        
        for txt_are in txt_ares:

            x = txt_are[0]
            y = txt_are[1]
            w = txt_are[2]
            h = txt_are[3]
            
            cv.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 1)

        rst_img = img
        #rst_img = _

    except Exception as ex:

        rst_img = img

        print("TargetAIROCR.detectText", ex)
        
    return rst_img


def extractTextArea(img, kernel_size = (1, 35), w_limit = (0, 30), h_limit = (0, 35), thr = 250):

    rst_img = []

    try:

        txt_ares, _ = getDetectTextLocation(img, kernel_size, w_limit, h_limit, thr)

        txt_ares = sorted(txt_ares, key = itemgetter(1, 0))

        tmp_img = np.ones((img.shape[0], img.shape[1], img.shape[2]), np.uint8) * 255
        
        for txt_are in txt_ares:

            x = txt_are[0]
            y = txt_are[1]
            w = txt_are[2]
            h = txt_are[3]
            
            tmp_img[y : y + h, x : x + w] = img[y : y + h, x : x + w]

        rst_img = tmp_img

    except Exception as ex:

        rst_img = img

        print("TargetAIROCR.extractTextArea", ex)
        
    return rst_img


def extractDocStructure(img, org_img, morph_size = (8, 8), thr = 250):

    rst_img = []

    try:

        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

        #blur = cv.GaussianBlur(gray, (5, 5), 0)
        blur = gray
        
        thr_img = cv.threshold(blur, thr, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)[1]
        struct = cv.getStructuringElement(cv.MORPH_RECT, morph_size)
        tmp_img = cv.dilate(~thr_img, struct, anchor = (-1, -1), iterations = 1)

        #cv.imshow(".", tmp_img)

        contours, hierarchy = cv.findContours(tmp_img, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)

        min_text_height_list = 10
        max_text_height_list = 300

        boxes = []
        for contour in contours:

            box = cv.boundingRect(contour)
            h = box[3]

            if min_text_height_list < h < max_text_height_list:

                boxes.append(box)

        #print(len(boxes))
        #print(boxes)

        cell_threshold = 10
        min_columns = 0

        rows = {}
        cols = {}

        for box in boxes:

            (x, y, w, h) = box

            col_key = x // cell_threshold
            row_key = y // cell_threshold

            cols[row_key] = [box] if col_key not in cols else cols[col_key] + [box]
            rows[row_key] = [box] if row_key not in rows else rows[row_key] + [box]

        table_cells = list(filter(lambda r: len(r) >= min_columns, rows.values()))
        table_cells = [list(sorted(tb)) for tb in table_cells]
        table_cells = list(sorted(table_cells, key = lambda r: r[0][1]))

        if table_cells is None or len(table_cells) <= 0:

            return img

        max_last_col_width_row = max(table_cells, key = lambda b: b[-1][2])
        max_x = max_last_col_width_row[-1][0] + max_last_col_width_row[-1][2]

        max_last_row_height_box = max(table_cells[-1], key = lambda b: b[3])
        max_y = max_last_row_height_box[1] + max_last_row_height_box[3]

        hor_lines = []
        ver_lines = []

        for box in table_cells:

            x = box[0][0]
            y = box[0][1]
            
            hor_lines.append((x, y, max_x, y))

        for box in table_cells[0]:

            x = box[0]
            y = box[1]

            ver_lines.append((x, y, x, max_y))

        (x, y, w, h) = table_cells[0][-1]
        ver_lines.append((max_x, y, max_x, max_y))
        
        (x, y, w, h) = table_cells[0][0]
        hor_lines.append((x, max_y, max_x, max_y))

        hor_lines = hor_lines[1:6]

        # 가로선 긋기
        for line in hor_lines:

            [x1, y1, x2, y2] = line
            #cv.line(org_img, (5, y1 - 5), (img.shape[1] - 5, y2 - 5), (0, 0, 255), 1)

        # 양쪽 세로선 긋기
        #cv.line(org_img, (5, hor_lines[0][1] - 5), (5, hor_lines[len(hor_lines)-1][1] - 5), (0, 0, 255), 1)
        #cv.line(org_img, (img.shape[1] - 5, hor_lines[0][1] - 5), (img.shape[1] - 5, hor_lines[len(hor_lines)-1][1] - 5), (0, 0, 255), 1)

        # 중간 세로선 긋기
        #cv.line(org_img, (ver_lines[0][0], hor_lines[0][1] - 5), (ver_lines[0][2], hor_lines[len(hor_lines)-1][1] - 5), (0, 0, 255), 1)

        # 필요 영역만 자르기
        x = 5
        y = hor_lines[0][1] - 5
        w = (img.shape[1] - 5) - 5 + 3
        h = (hor_lines[len(hor_lines)-1][1] - 5) - (hor_lines[0][1] - 5) + 3
        
        rst_img = org_img[y : y + h, x : x + w]
        
    except Exception as ex:

        rst_img = img

        print("TargetAIROCR.extractDocStructure", ex)
        
    return rst_img


def drawKmeans(img, K = 2):

    rst_img = []

    try:
        """
        hsv = cv.cvtColor(img, cv.COLOR_RGB2HSV)
        h, s, v = cv.split(hsv)

        clahe = cv.createCLAHE(clipLimit = 2.0, tileGridSize = (16, 16))
        v = clahe.apply(v)

        hsv = cv.merge([h, s, v])
        img = cv.cvtColor(hsv, cv.COLOR_HSV2RGB)
        """

        Z = img.reshape((-1, 3))
        Z = np.float32(Z)

        criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        ret, label, center = cv.kmeans(Z, K, None, criteria, 10, cv.KMEANS_RANDOM_CENTERS)

        center = np.uint8(center)
        res = center[label.flatten()]

        rst_img = res.reshape((img.shape))

    except Exception as ex:

        rst_img = img

        print("TargetAIROCR.drawKmeans", ex)
        
    return rst_img


    

def getResult(img):

    rst_strs = []
    print("getResult0")
    try:
        # Image Scale 조정
        scale_rate = 1000 / img.shape[1]
        img = cv.resize(img, dsize = (0, 0), fx = scale_rate, fy = scale_rate, interpolation = cv.INTER_LINEAR)

        org_img = img.copy()

        img = removeWaterMark(img)

        img = drawKmeans(img, K = 2)

        # Text 영역 추출
        w_limit = (15, img.shape[1])
        h_limit = (15, 45)
        img = extractTextArea(img, kernel_size = (5, 75), w_limit = w_limit, h_limit = h_limit, thr = 250)

        # 문서내 읽을 영역 추출
        img = extractDocStructure(img, org_img, morph_size = (60, 4), thr = 250)
        img = cv.resize(img, dsize = (0, 0), fx = 0.7, fy = 0.7, interpolation = cv.INTER_LINEAR)

        # Text 결과 
        tmp_str = getText(img).replace(" " ,"").replace("\n\n" ,"\n")
        tmp_strs = tmp_str.split("\n")

        taxpaperType = tmp_strs[0] if len(tmp_strs) > 0 else ""
        identificationNum = tmp_strs[1].split(":")[1] if len(tmp_strs) > 1 and len(tmp_strs[1].split(":")) > 1 else ""
        companyName = tmp_strs[2].split(":")[1] if len(tmp_strs) > 2 and len(tmp_strs[2].split(":")) > 1 else ""
        onnerName = tmp_strs[3].split(":")[1] if len(tmp_strs) > 3 and len(tmp_strs[3].split(":")) > 1 else ""
        

        # 후처리
        taxpaperType = taxpaperType.replace("(","").replace(")","")
        onnerName = onnerName.replace("생년월일","").replace("생년뭘일","").replace("주민등록번호","")
        
        rst_strs = [taxpaperType, identificationNum, companyName, onnerName]

    except Exception as ex:

        rst_strs = []
        
        print("TargetAIROCR.getResult", ex)
        
    return rst_strs

"""
def getImage(img_path, scale_rate = 0):

    rst_img = []

    try:
        
        stream = open(img_path.encode("utf-8"), "rb")
        bytes = bytearray(stream.read())
        rst_img = cv.imdecode(np.asarray(bytes, dtype = np.uint8), cv.IMREAD_COLOR)
    
        if scale_rate != 0:
            
            rst_img = cv.resize(rst_img, dsize = (0, 0), fx = scale_rate, fy = scale_rate, interpolation = cv.INTER_LINEAR)

    except Exception as ex:

        print("TargetAIROCR.getImage", ex)
        
    return rst_img


for txt in getResult(getImage("C:/Users/KTDS/Desktop/TargetAIR/V1.3/사업자등록증/bf00000001.jpg")):

    print(txt)
"""
