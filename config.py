import configparser

def read_config():
        # read file
    PATH_SECTION = 'path_info'
    DOCUMENATION_SECTION = 'documentation_index'
    config = configparser.ConfigParser()
    ini_path = 'config.ini'
    config.read(ini_path, encoding="utf-8")
    
    documentation_index = dict()
    for k in config[DOCUMENATION_SECTION]:
        documentation_index[k] = config[DOCUMENATION_SECTION][k]
    
    result = {
        'modelUri': config[PATH_SECTION]['tf_server_url'],
        'documentation_index': documentation_index     
            
    }
    return result    