FROM python:3.8.5


COPY . /app
WORKDIR /app

COPY tesseract/tessdata .

RUN apt-get update
RUN apt-get install tesseract-ocr -y
#RUN apt-get install tesseract-ocr-kor -y
RUN apt-get install libgl1-mesa-glx -y
#RUN apt-get install tesseract-ocr-script-hang tesseract-ocr-script-hang-vert
RUN pip install pytesseract
RUN pip install -r requirements.txt



ENV TESSDATA_PREFIX ${BASE_DIR}

EXPOSE 5000

CMD ["python",  "./app.py", "runserver", "0.0.0.0:5000"]
