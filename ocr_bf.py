from flask import request
from flask import Blueprint
from flask_restful import Resource
import numpy as np
from PIL import Image
import requests
import json
import logging
from config import read_config

from TargetAIROCR import getResult
import cv2 as cv

OcrBusinessForm = Blueprint('ocr_bf', __name__)
config = read_config()
@OcrBusinessForm.route('')
class Ocr_BF(Resource):    
    def post(self): #post 요청시 json 형태로 반환

        # key=files values stream으로 처리
        # RGB, 사이즈 전처리
        img = np.array(Image.open(request.files['file'].stream))
        print("Ocr_BF0")
        try:
            # Target AIR OCR 호출
            rst_strs = getResult(img)
            print("Ocr_BF0")
            # 응답값 반환
            return [{
                            'no':'1',
                            'category': '사업자유형',
                            'contents':rst_strs[0],
                            'accuracy':'100%'
                        },
                    {
                            'no':'2',
                            'category': '등록번호',
                            'contents':rst_strs[1],
                            'accuracy':'100%'
                        },
                    {
                            'no':'3',
                            'category': '법인명(단체명)',
                            'contents':rst_strs[2],
                            'accuracy':'100%'
                        },
                    {
                            'no':'4',
                            'category': '대표자',
                            'contents':rst_strs[3],
                            'accuracy':'100%'
                        }]

        except Exception as e:
            logging.error(f"{e} : Return result to manually classification as raising error")
            return [{
                            'no':'-',
                            'category': '-',
                            'contents':'-',
                            'accuracy':'-'
                        }]
